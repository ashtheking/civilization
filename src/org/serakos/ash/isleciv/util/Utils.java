package org.serakos.ash.isleciv.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.serakos.ash.isleciv.Civilization;
import org.serakos.ash.isleciv.World;
import org.serakos.ash.isleciv.data.tile.Tile;

public class Utils
{

	public static Set<Location> neighbors(Location[][] world, Location current) {
		return neighbors(world, current, 1);
	}

	private static Set<Location> neighborSet = Collections.synchronizedSet(new HashSet<>());

	public static Set<Location> neighbors(Location[][] world, Location current, int n) {
		neighborSet.clear();
		for (int i = -n; i <= n; i++)
			for (int j = -n; j <= n; j++)
				neighborSet.add(world[valid(world, current.getX() + i)][valid(world, current.getY() + j)]);
		return neighborSet;
	}

	public static int valid(Location[][] world, int n) {
		return Math.min(Math.max(n, 0), world.length - 1);
	}

	public static int distSqrd(int ax, int ay, int bx, int by) {
		return ((ax - bx) * (ax - bx) + (ay - by) * (ay - by));
	}

	public static void printArray(int[][] array) {
		Arrays.asList(array).stream().map(Arrays::toString).forEach(System.out::println);
	}

	public static void printArray(double[][] array) {
		Arrays.asList(array).stream().map(Arrays::toString).forEach(System.out::println);
	}

	public static Stream<Tile> streamWorld(World world) {
		return Arrays.stream(world.worldMap).flatMap(Arrays::stream);
	}

	public static int randDice(Random random, int n, int d) {
		if (random == null || n == 0 || d == 0)
			return 0;
		return IntStream.range(0, n).map(i -> random.nextInt(d) + 1).sum();
	}

	public static class Time
	{
		long time, time2;
		int num = 0;

		public void startTime(int n, String s) {
			this.num = n;
			time = System.currentTimeMillis();
			System.out.println("[" + time + "] " + getTabs() + " " + s);
			if (n < 3)
				Civilization.setTitle(s);
		}

		public void endTime() {
			time2 = System.currentTimeMillis();
			System.out.println("[" + time2 + "] " + getTabs() + " Done. [" + (time2 - time) + "]");
		}

		private String getTabs() {
			StringBuilder s = new StringBuilder();
			for (int x = 0; x < num; x++)
				s.append("\t");
			return s.toString();
		}
	}
}
