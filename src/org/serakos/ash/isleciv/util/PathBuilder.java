package org.serakos.ash.isleciv.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.PriorityQueue;

import org.serakos.ash.isleciv.World;
import org.serakos.ash.isleciv.data.tile.Structure;
import org.serakos.ash.isleciv.data.tile.TileData;

public class PathBuilder
{
	private World refWorld;
	private Point[][] world;
	private PriorityQueue<Point> frontier;
	private List<Point> touched;

	public PathBuilder(World world)
	{
		this.refWorld = world;
		this.world = new Point[world.worldMap.length][world.worldMap[0].length];
		for (int x = 0; x < this.world.length; x++)
			for (int y = 0; y < this.world[x].length; y++)
				this.world[x][y] = new Point(x, y);
		this.frontier = new PriorityQueue<>();
		this.touched = new LinkedList<>();
	}

	public void buildPath(org.serakos.ash.isleciv.data.tile.Tile[] tiles) {
		Point start = world[tiles[0].getX()][tiles[0].getY()];
		Point end = world[tiles[1].getX()][tiles[1].getY()];

		resetCost();
		start.costSoFar = 0;
		start.cameFrom = null;
		refWorld.focusX = start.getX();
		refWorld.focusY = start.getY();

		frontier.add(start);
		Utils.Time time = new Utils.Time();
		time.startTime(3, "Calculating path.");
		while (!frontier.isEmpty()) {
			Point current = frontier.poll();
			if (current == null || current == end)
				break;
			Utils.neighbors(world, current).forEach((n) -> {
				Point next = (Point) n;
				touched.add(next);
				int newCost = current.costSoFar + cost(current, next);
				if (newCost < next.costSoFar) {
					next.costSoFar = newCost;
					next.priority = newCost // Switched to A*
							+ Utils.distSqrd(current.getX(), current.getY(), next.getX(), next.getY());
					next.cameFrom = current;
					frontier.add(next);
				}
			});
		}
		time.endTime();

		Point p = end;
		time.startTime(3, "Constructing path.");
		while (p != null) {
			org.serakos.ash.isleciv.data.tile.Tile t = getTile(p);
			if (t.get(TileData.STRUCTURE) == Structure.EMPTY.ordinal()) {
				t.set(TileData.WEIGHT, t.get(TileData.WEIGHT) / 2);
				t.set(TileData.STRUCTURE, Structure.ROAD);
			}
			p = p.cameFrom;
		}
		time.endTime();
	}

	private void resetCost() {
		Utils.Time time = new Utils.Time();
		time.startTime(3, "Resetting cost.");
		frontier.clear();
		touched.parallelStream().filter(Objects::nonNull).forEach(t -> {
			t.priority = 0;
			t.costSoFar = Integer.MAX_VALUE;
			t.cameFrom = null;
		});
		touched.clear();
		time.endTime();
	}

	//
	private int cost(Point c, Point n) {
		org.serakos.ash.isleciv.data.tile.Tile current = getTile(c), next = getTile(n);
		return next.get(TileData.WEIGHT) + (next.belowSeaLevel(0,
				(int) Math.pow(current.get(TileData.HEIGHT) - next.get(TileData.HEIGHT), 2.75)));
	}

	private org.serakos.ash.isleciv.data.tile.Tile getTile(Point p) {
		return refWorld.worldMap[p.getX()][p.getY()];
	}

	private static class Point extends Location implements Comparable<Point>
	{
		public int costSoFar;
		public int priority;
		public Point cameFrom;

		public Point(int x, int y)
		{
			super(x, y);
			costSoFar = Integer.MAX_VALUE;
			priority = 0;
			cameFrom = null;
		}

		@Override
		public int compareTo(Point t) {
			return this.priority - t.priority;
		}
	}
}
