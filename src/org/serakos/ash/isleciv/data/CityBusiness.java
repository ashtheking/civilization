package org.serakos.ash.isleciv.data;

public enum CityBusiness
{
	SHOEMAKERS("Shoemakers", 150),
	BUTCHERS("Butchers", 1200),
	FURRIERS("Furriers", 250),
	FISHMONGERS("Fishmongers", 1200),
	MAIDSERVANTS("Maidservants", 250),
	BEER("Beer Sellers", 1400),
	TAILORS("Tailors", 250),
	BUCKLE("Buckle Makers", 1400),
	BARBERS("Barbers", 350),
	PLASTERERS("Plasterers", 1400),
	JEWELERS("Jewelers", 400),
	SPICE("Spice Merchants", 1400),
	TAVERNS("Taverns/Restaurants", 400),
	BLACKSMITHS("Blacksmiths", 1500),
	CLOTHES("Clothiers", 400),
	PAINTERS("Painters", 1500),
	PASTRYCOOKS("Pastrycooks", 500),
	DOCTORS("Doctors", 1700),
	MASONS("Masons", 500),
	ROOFERS("Roofers", 1800),
	CARPENTERS("Carpenters", 550),
	LOCKSMITHS("Locksmiths", 1900),
	WEAVERS("Weavers", 600),
	BATHERS("Bathers", 1900),
	CHANDLERS("Chandlers", 700),
	ROPEMAKERS("Ropemakers", 1900),
	MERCERS("Mercers", 700),
	INNS("Inns", 2000),
	COOPERS("Coopers", 700),
	TANNERS("Tanners", 2000),
	BAKERS("Bakers", 800),
	COPYISTS("Copyists", 2000),
	WATERCARRIERS("Watercarriers", 850),
	SCULPTORS("Sculptors", 2000),
	SCABBARDMAKERS("Scabbardmakers", 850),
	RUGMAKERS("Rugmakers", 2000),
	WINE("Wine Sellers", 900),
	HARNESS("Harness Makers", 2000),
	HATMAKERS("Hatmakers", 950),
	BLEACHERS("Bleachers", 2100),
	SADDLERS("Saddlers", 1000),
	HAY("Hay Merchants", 2300),
	CHICKEN("Chicken Butchers", 1000),
	CUTLERS("Cutlers", 2300),
	PURSEMAKERS("Pursemakers", 1100),
	GLOVEMAKERS("Glovemakers", 2400),
	WOODSELLERS("Woodsellers", 2400),
	WOODCARVERS("Woodcarvers", 2400),
	BOOKSELLERS("Booksellers", 6300),
	BOOKBINDERS("Bookbinders", 3000),
	ILLUMINATORS("Illuminators", 3900),
	// Ending semicolon
	;

	public String displayName;
	public int supportValue;

	CityBusiness(String d, int sv)
	{
		this.displayName = d;
		this.supportValue = sv;
	}
}
