package org.serakos.ash.isleciv.data.tile;

public enum TileData
{
	KINGDOM,
	STRUCTURE,
	SETTLE_ID,
	POPULATION_CHANCE,
	HEIGHT,
	WEIGHT,
	TEMPERATURE,
	OCEAN_DOWNWIND,
	SEASONAL_CHANGE,
	//
	;
}
