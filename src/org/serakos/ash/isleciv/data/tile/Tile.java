package org.serakos.ash.isleciv.data.tile;

import java.util.Arrays;

import org.serakos.ash.isleciv.Constants;
import org.serakos.ash.isleciv.util.Location;

public class Tile extends Location
{
	private short[] tiledata = new short[TileData.values().length];

	public Tile(int x, int y)
	{
		super(x, y);
		this.set(TileData.STRUCTURE, Structure.EMPTY.ordinal());
		this.set(TileData.SETTLE_ID, -1);
		this.set(TileData.WEIGHT, 1);
	}

	public short get(TileData data) {
		return tiledata[data.ordinal()];
	}

	public void set(TileData t, short v) {
		this.tiledata[t.ordinal()] = v;
	}

	public void set(TileData t, Number v) {
		this.set(t, v.shortValue());
	}

	public void set(TileData t, Structure v) {
		this.set(t, v.ordinal());
	}

	public <T> T belowSeaLevel(T a, T b) {
		return this.get(TileData.HEIGHT) < Constants.SEA_HEIGHT ? a : b;
	}

	@Override
	public String toString() {
		return "Tile [x=" + getX() + ", y=" + getY() + Arrays.stream(TileData.values())
				.map((t) -> t.toString() + "=" + get(t)).reduce("", (a, b) -> a + ", " + b) + "]";
	}

	public Tile[] makePair(Tile other) {
		return new Tile[] { this, other };
	}
}
