package org.serakos.ash.isleciv.data.tile;

public class GenLayer
{
	private TileData set;
	private Supplier val;

	public GenLayer(TileData setter, Supplier value)
	{
		this.set = setter;
		this.val = value;
	}

	public void act(Tile t, Integer x, Integer y) {
		t.set(this.set, this.val.supply(t, x, y));
	}

	public TileData getData() {
		return set;
	}

	@FunctionalInterface
	public interface Supplier
	{
		public Number supply(Tile t, Integer x, Integer y);
	}
}
