package org.serakos.ash.isleciv.data.mst;

import org.serakos.ash.isleciv.data.City;
import org.serakos.ash.isleciv.data.tile.Tile;
import org.serakos.ash.isleciv.util.Utils;

public class CityPair implements Comparable<CityPair>
{
	public int priority;
	public City one;
	public City two;

	public CityPair(City one, City two)
	{
		this.one = one;
		this.two = two;
		int pop = one.population * two.population;
		double dist = Utils.distSqrd(one.x, one.y, two.x, two.y);
		this.priority = (int) Math.ceil((pop / dist) * 50);
	}

	@Override
	public int compareTo(CityPair t) {
		return this.priority - t.priority;
	}

	public Tile[] getTiles() {
		return new Tile[] { one.getTile(), two.getTile() };
	}

	@Override
	public String toString() {
		return one.name + " and " + two.name + " at priority " + priority + ".";
	}
}
