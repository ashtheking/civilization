package org.serakos.ash.isleciv.data.mst;

import java.util.ArrayList;
import java.util.List;

public class MinimumTree
{
	// Thanks to:
	// https://www.geeksforgeeks.org/greedy-algorithms-set-2-kruskals-minimum-spanning-tree-mst/

	// A class to represent a subset for union-find
	static class subset
	{
		int parent, rank;
	}

	// A utility function to find set of an element i
	// (uses path compression technique)
	static int find(subset subsets[], int i) {
		// find root and make root as parent of i (path compression)
		if (subsets[i].parent != i)
			subsets[i].parent = find(subsets, subsets[i].parent);

		return subsets[i].parent;
	}

	// A function that does union of two sets of x and y
	// (uses union by rank)
	static void union(subset subsets[], int x, int y) {
		int xroot = find(subsets, x);
		int yroot = find(subsets, y);

		// Attach smaller rank tree under root of high rank tree
		// (Union by Rank)
		if (subsets[xroot].rank < subsets[yroot].rank)
			subsets[xroot].parent = yroot;
		else if (subsets[xroot].rank > subsets[yroot].rank)
			subsets[yroot].parent = xroot;

		// If ranks are same, then make one as root and increment
		// its rank by one
		else {
			subsets[yroot].parent = xroot;
			subsets[xroot].rank++;
		}
	}

	public static List<CityPair> buildTree(int V, List<CityPair> edge) {
		List<CityPair> result = new ArrayList<>();  // Tnis will store the resultant MST
		int e = 0;  // An index variable, used for result[]
		int i = 0;  // An index variable, used for sorted edges

		// Allocate memory for creating V ssubsets
		subset subsets[] = new subset[V];
		for (i = 0; i < V; ++i)
			subsets[i] = new subset();

		// Create V subsets with single elements
		for (int v = 0; v < V; ++v) {
			subsets[v].parent = v;
			subsets[v].rank = 0;
		}

		i = 0;  // Index used to pick next edge

		// Number of edges to be taken is equal to V-1
		while (e < V - 1) {
			// Step 2: Pick the smallest edge. And increment
			// the index for next iteration
			CityPair next_edge = edge.get(i++);

			int x = find(subsets, next_edge.one.id);
			int y = find(subsets, next_edge.two.id);

			// If including this edge does't cause cycle,
			// include it in result and increment the index
			// of result for next edge
			if (x != y) {
				result.add(next_edge);
				union(subsets, x, y);
				e++;
			}
			// Else discard the next_edge
		}

		return result;
	}
}
