package org.serakos.ash.isleciv.data;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.serakos.ash.isleciv.Constants;
import org.serakos.ash.isleciv.data.mst.CityPair;
import org.serakos.ash.isleciv.data.tile.Tile;

public class City
{
	public Kingdom kingdom;
	public short id;
	public int x;
	public int y;
	public int population;
	public String name;
	public List<Business> businesses;

	public City(Kingdom kng, short id, int x, int y, String name)
	{
		this.kingdom = kng;
		this.id = id;
		this.x = x;
		this.y = y;
		this.name = name;
		this.population = -1;
	}

	public Tile getTile() {
		return kingdom.world.worldMap[x][y];
	}

	public String getType() {
		return population > Constants.CITY_POP ? "City" : "Town";
	}

	@Override
	public String toString() {
		return getType() + " [id=" + id + ", x=" + x + ", y=" + y + ", population=" + population + ", name="
				+ name + "]";
	}

	public CityPair makePair(City other) {
		return new CityPair(this, other);
	}

	public Tile[] makeTilePair(City other) {
		return getTile().makePair(other.getTile());
	}

	public void calculateBusinesses() {
		businesses = Arrays.stream(CityBusiness.values()) // Stream
				.map(b -> new Business(b, (double) population / b.supportValue)) // Create
				.filter(b -> b.number > 0) // Only if there's at least one
				.sorted((a, b) -> b.number - a.number) // Sort it
				.collect(Collectors.toList()); // Collect`
	}

	public class Business
	{
		CityBusiness business;
		int number;

		public Business(CityBusiness b, double n)
		{
			this.business = b;
			this.number = (int) n;
			if (kingdom.world.random.nextDouble() < (n - number))
				this.number++;
		}

		@Override
		public String toString() {
			return number + " " + business.displayName;
		}
	}
}
