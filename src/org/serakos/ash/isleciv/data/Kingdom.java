package org.serakos.ash.isleciv.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.serakos.ash.isleciv.Constants;
import org.serakos.ash.isleciv.World;
import org.serakos.ash.isleciv.data.mst.CityPair;
import org.serakos.ash.isleciv.data.mst.MinimumTree;
import org.serakos.ash.isleciv.data.tile.Structure;
import org.serakos.ash.isleciv.data.tile.Tile;
import org.serakos.ash.isleciv.data.tile.TileData;
import org.serakos.ash.isleciv.util.PathBuilder;
import org.serakos.ash.isleciv.util.Utils;
import org.serakos.language.Language;

public class Kingdom
{
	public final short kingdomId;
	protected World world;

	public Language language;
	public int voronoiX;
	public int voronoiY;

	public String name;
	public long sqrMiles;
	public int arability;
	public long totalPop;

	public City largest;

	private int popSum;
	private PathBuilder builder;
	private List<City> cities;
	private List<CityPair> cityBuildList;
	public boolean roadsGenerated;

	public Kingdom(World world, short kid)
	{
		this.world = world;
		this.kingdomId = kid;
		this.voronoiX = world.random.nextInt(world.worldMap.length);
		this.voronoiY = world.random.nextInt(world.worldMap[this.voronoiX].length);
		this.cities = new ArrayList<>();
	}

	public void generate() {
		Utils.Time time = new Utils.Time();
		Utils.Time time2 = new Utils.Time();

		language = Language.randomLanguage();
		language.initializeStandards();
		name = Language.capitalize(language.getName("Kingdom Name"));

		time.startTime(1, "Creating Kingdom of " + name);
		builder = new PathBuilder(world);
		popSum = Utils.streamWorld(world).filter(this::isOurs)
				.mapToInt(t -> t.get(TileData.POPULATION_CHANCE)).sum();

		sqrMiles = Utils.streamWorld(world).filter(this::isOurs).count();
		arability = Utils.randDice(world.random, 6, 4) * 5; // 6d4 * 5 [30 <= x <= 120, avg: 75 +- 13.7]
		totalPop = arability * sqrMiles;

		time2.startTime(2, "Generating Settlements");
		genCities();
		Collections.sort(cities, this::citySort);
		cities.forEach(this::buildCity);
		time2.endTime();

		time2.startTime(2, "Compiling Roads for " + name);
		cityBuildList = IntStream.range(0, cities.size()) // For all cities
				.mapToObj(cities::get) // Get the city
				.flatMap(i -> // For each city
				IntStream.range(i.id + 1, cities.size()) // For all other cities
						.mapToObj(cities::get) // Get that city
						.map(i::makePair)  // Make an edge
						.filter(a -> a.priority != 0) // Ignore dist-zero
						.sorted() // Sort the list of edges
				).collect(Collectors.toList()); // Compile the sorted list of edges
		// Build the minimum spanning tree off the sorted list
		cityBuildList = MinimumTree.buildTree(cities.size(), cityBuildList);
		time2.endTime();

		time2.startTime(2, "Giving citizens jobs.");
		cities.forEach(City::calculateBusinesses);
		time2.endTime();

		time.endTime();
	}

	public boolean genRoads() {
		if (cityBuildList == null || cityBuildList.isEmpty())
			return true;
		CityPair cityPair = cityBuildList.get(0);
		Utils.Time time = new Utils.Time();
		time.startTime(2, "Building Roads connecting " + cityPair);
		builder.buildPath(cityBuildList.remove(0).getTiles());
		time.endTime();
		if (cityBuildList.isEmpty())
			roadsGenerated = true;
		return false;
	}

	private void genCities() {
		Utils.Time time3 = new Utils.Time();

		time3.startTime(3, "Generating Capital City");
		cities.add(largest = randomCity(0));
		largest.population = (int) (Math.sqrt(totalPop) * (Utils.randDice(world.random, 2, 4) + 10));
		time3.endTime();

		int numCities = (int) Math.pow(largest.population / (double) Constants.CITY_POP, 1 / 1.07) + 1;
		int numTowns = numCities * (world.random.nextInt(4) + 1) + 1;

		time3.startTime(3, "Generating " + numCities + " Cities");
		IntStream.range(1, numCities).mapToObj(this::randomCity).forEach(cities::add);
		time3.endTime();

		time3.startTime(3, "Generating " + numTowns + " Towns");
		IntStream.range(cities.size(), numTowns + cities.size()).mapToObj(this::randomCity)
				.forEach(cities::add);
		time3.endTime();
	}

	private City randomCity(int i) {
		double u = world.random.nextInt(popSum), t = 0;
		for (int x = 0; x < world.worldMap.length; x++)
			for (int y = 0; y < world.worldMap[x].length; y++)
				if (world.worldMap[x][y].get(TileData.KINGDOM) == kingdomId)
					if ((t += world.worldMap[x][y].get(TileData.POPULATION_CHANCE)) >= u)
						return new City(this, (short) i, x, y, Language.capitalize(language.getName("" + i)));
		return null;
	}

	private void buildCity(City city) {
		city.population = (int) (largest.population / Math.pow(city.id + 1, 1.07));
		double constant = city.population > Constants.CITY_POP ? Constants.CITY_SIZE : Constants.TOWN_SIZE;
		int n = (int) Math.max(Math.ceil(city.population / (double) largest.population * constant), 1);
		Utils.neighbors(world.worldMap, city.getTile(), n).forEach(l -> {
			Tile t = (Tile) l;
			t.set(TileData.STRUCTURE, Structure.CITY);
			t.set(TileData.SETTLE_ID, city.id);
			t.set(TileData.WEIGHT, 0);
		});
	}

	private int citySort(City a, City b) {
		return (world.worldMap[a.x][a.y].get(TileData.POPULATION_CHANCE)
				+ world.worldMap[b.x][b.y].get(TileData.POPULATION_CHANCE)) * 10;
	}

	public City getCity(int cityId) {
		return cityId > -1 && cityId < cities.size() ? cities.get(cityId) : null;
	}

	public double getDist(int x, int y) {
		return Utils.distSqrd(x, y, voronoiX, voronoiY);
	}

	public boolean isOurs(Tile t) {
		return t.get(TileData.KINGDOM) == kingdomId;
	}

	public Tile[] makeTilePair(Kingdom other) {
		return largest.makeTilePair(other.largest);
	}
}
