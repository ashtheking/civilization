package org.serakos.ash.isleciv;

public enum WorldState
{
	INIT("Creating World"),
	TILES("Generating World"),
	KINGDOMS("Generating Kingdoms"),
	KINGDOM_ROADS("Building Roads"),
	CONNECT("Connecting Kingdoms"),
	FINISHING("Finishing Up"),
	DONE("");

	public final String output;

	WorldState(String s)
	{
		this.output = s;
	}
}
