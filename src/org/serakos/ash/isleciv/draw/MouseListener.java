package org.serakos.ash.isleciv.draw;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;

import org.serakos.ash.isleciv.data.City;
import org.serakos.ash.isleciv.data.tile.Tile;
import org.serakos.ash.isleciv.data.tile.TileData;

public class MouseListener extends MouseAdapter
{
	private final WorldPanel worldPanel;

	MouseListener(WorldPanel worldPanel)
	{
		this.worldPanel = worldPanel;
	}

	@Override
	public void mouseClicked(MouseEvent event) {
		int x = event.getX() / 2;
		int y = event.getY() / 2;
		Tile t = this.worldPanel.world.worldMap[x][y];
		if (t.get(TileData.SETTLE_ID) != -1)
			jFrame(this.worldPanel.world.kingdoms.get(t.get(TileData.KINGDOM))
					.getCity(t.get(TileData.SETTLE_ID)));
	}

	public void jFrame(City city) {
		if (this.worldPanel.frames.get(city) == null) {
			Tile tile = city.getTile();
			String type = city == city.kingdom.largest ? "Capital City " : (city.getType() + " of ");

			JFrame frame = new JFrame(city.name + " Info");
			JPanel panel = new JPanel();
			panel.setLayout(new GridLayout(2, 1));

			JPanel firstPanel = new JPanel();
			firstPanel.setBorder(new TitledBorder(type + city.name));
			firstPanel.setLayout(new GridLayout(4, 1));
			firstPanel.add(new JLabel("Kingdom of " + city.kingdom.name));
			firstPanel.add(new JLabel("Population: " + city.population));
			firstPanel.add(new JLabel("Elevation: " + tile.get(TileData.HEIGHT) + "m"));
			firstPanel.add(new JLabel("Annual Temp.: " + tile.get(TileData.TEMPERATURE) + "ºF +/- "
					+ Math.abs(tile.get(TileData.SEASONAL_CHANGE)) + "ºF"));
			panel.add(firstPanel);

			JPanel secondPanel = new JPanel();
			secondPanel.setBorder(new TitledBorder("City Businesses: "));
			secondPanel.setLayout(new GridLayout(city.businesses.size(), 1));
			city.businesses.forEach(b -> secondPanel.add(new JLabel(b.toString())));
			panel.add(new JScrollPane(secondPanel));

			frame.setContentPane(panel);
			frame.setSize(250, 400);
			frame.setResizable(false);
			frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
			this.worldPanel.frames.put(city, frame);
		}
		this.worldPanel.frames.get(city).setVisible(true);
	}
}