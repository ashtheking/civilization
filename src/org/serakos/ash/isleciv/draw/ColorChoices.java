package org.serakos.ash.isleciv.draw;

import java.awt.Color;

import org.serakos.ash.isleciv.Constants;
import org.serakos.ash.isleciv.World;
import org.serakos.ash.isleciv.data.City;
import org.serakos.ash.isleciv.data.Kingdom;
import org.serakos.ash.isleciv.data.tile.Structure;
import org.serakos.ash.isleciv.data.tile.Tile;
import org.serakos.ash.isleciv.data.tile.TileData;

public enum ColorChoices
{

	STANDARD((panel, world, x, y) -> {
		Tile tile = world.worldMap[x][y];
		short s = tile.get(TileData.STRUCTURE), k = tile.get(TileData.KINGDOM), h = tile.get(TileData.HEIGHT),
				t = tile.get(TileData.TEMPERATURE), c = tile.get(TileData.SEASONAL_CHANGE),
				w = tile.get(TileData.OCEAN_DOWNWIND);
		int n = (int) Math.min(Math.pow(h / 5 - h % 5, 2), 255);

		if (s == Structure.ROAD.ordinal())
			return Color.white;
		if (s == Structure.CITY.ordinal()) {
			City ci = world.kingdoms.get(k).getCity(tile.get(TileData.SETTLE_ID));
			if (ci == null)
				return Color.black;
			n = ci.population;
			return new Color(Math.min(n * 3, 255), 0, 0);
		}
		if (x > 0 && world.worldMap[x - 1][y].get(TileData.KINGDOM) != k)
			return Color.black;
		if (y > 0 && world.worldMap[x][y - 1].get(TileData.KINGDOM) != k)
			return Color.black;
		if (x < world.worldMap.length - 1 && world.worldMap[x + 1][y].get(TileData.KINGDOM) != k)
			return Color.black;
		if (y < world.worldMap.length - 1 && world.worldMap[x][y + 1].get(TileData.KINGDOM) != k)
			return Color.black;

		if (t < panel.cold) {
			if (h < panel.heightAverage)
				return new Color(n * 2 / 3, n * 2 / 3, n * 2 / 3); // alpine
			return new Color(n, n, n); // tundra
		}
		else if (t > panel.hot) {
			if (w > panel.windAverage)
				return new Color(n, n, n / 5); // desert
			return new Color(n / 9, n, n / 9); // tropical
		}
		if (w > panel.windAverage && c < panel.seasonAverage)
			return new Color(n / 5, n, n / 5); // continental
		return new Color(n / 2, n, n / 2); // oceanic
	}),

	HEIGHT((panel, world, x, y) -> {
		Tile tile = world.worldMap[x][y];
		short h = tile.get(TileData.HEIGHT);
		int n = (int) Math.min(Math.pow(h / 5 - h % 5, 2), 255);
		return new Color(0, n / 2, 0);
	}),

	TEMPERATURE((panel, world, x, y) -> {
		Tile tile = world.worldMap[x][y];
		short t = tile.get(TileData.TEMPERATURE);
		int i = (int) Math.min(Math.abs(Math.pow(t / 5 - t % 5, 3)), 255);
		return new Color(i, 0, 0);
	}),

	WIND((panel, world, x, y) -> {
		Tile tile = world.worldMap[x][y];
		short w = tile.get(TileData.OCEAN_DOWNWIND);
		int i = (int) Math.min(Math.abs(Math.pow(w / 5 - w % 5, 2)), 255);
		return new Color(i, i, 0);
	}),

	SEASONS((panel, world, x, y) -> {
		Tile tile = world.worldMap[x][y];
		short c = tile.get(TileData.SEASONAL_CHANGE);
		int i = (int) Math.min(Math.abs(Math.pow(c / 5, 2)), 255);
		return new Color(0, i, 0);
	}),

	KINGDOM((panel, world, x, y) -> {
		Tile tile = world.worldMap[x][y];
		short h = tile.get(TileData.HEIGHT), s = tile.get(TileData.STRUCTURE), k = tile.get(TileData.KINGDOM);
		int n = (int) Math.min(Math.pow(h / 5 - h % 5, 2), 200);
		int i = (int) ((200.0 * k) / world.kingdoms.size());
		Kingdom kng = world.kingdoms.get(k);

		for (int a = -3; a < 4; a++)
			for (int b = -3; b < 4; b++)
				if (x == kng.voronoiX + a && y == kng.voronoiY + b)
					return Color.green;

		if (s == Structure.ROAD.ordinal())
			return Color.white;
		if (s == Structure.CITY.ordinal()) {
			n = world.kingdoms.get(k).getCity(tile.get(TileData.SETTLE_ID)).population;
			return new Color(Math.min(n * 3, 255), 0, 0);
		}

		if (h < Constants.SEA_HEIGHT)
			return new Color(0, 0, n);
		return new Color(i, i, i);
	}),
	//
	;

	private ColorFunc color;

	ColorChoices(ColorFunc c)
	{
		this.color = c;
	}

	public ColorFunc func() {
		return this.color;
	}

	@FunctionalInterface
	public static interface ColorFunc
	{
		public Color getColor(WorldPanel panel, World world, int x, int y);
	}
}
