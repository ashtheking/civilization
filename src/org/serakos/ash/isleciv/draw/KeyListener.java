package org.serakos.ash.isleciv.draw;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;

import org.serakos.ash.isleciv.WorldState;

public class KeyListener extends KeyAdapter
{
	private final WorldPanel worldPanel;

	KeyListener(WorldPanel worldPanel)
	{
		this.worldPanel = worldPanel;
	}

	@Override
	public void keyPressed(KeyEvent key) {
		switch (key.getKeyCode()) {
		case KeyEvent.VK_R: {
			this.worldPanel.needsRedraw = true;
			break;
		}
		case KeyEvent.VK_ENTER: {
			if (this.worldPanel.world.state != WorldState.DONE)
				break;
			this.worldPanel.world.state = WorldState.INIT;
			this.worldPanel.frames.values().forEach(JFrame::dispose);
			this.worldPanel.frames.clear();
			this.worldPanel.needsRedraw = true;
			break;
		}
		case KeyEvent.VK_1: {
			this.worldPanel.setChoice(0);
			break;
		}
		case KeyEvent.VK_2: {
			this.worldPanel.setChoice(1);
			break;
		}
		case KeyEvent.VK_3: {
			this.worldPanel.setChoice(2);
			break;
		}
		case KeyEvent.VK_4: {
			this.worldPanel.setChoice(3);
			break;
		}
		case KeyEvent.VK_5: {
			this.worldPanel.setChoice(4);
			break;
		}
		case KeyEvent.VK_6: {
			this.worldPanel.setChoice(5);
			break;
		}
		case KeyEvent.VK_7: {
			this.worldPanel.setChoice(6);
			break;
		}
		case KeyEvent.VK_8: {
			this.worldPanel.setChoice(7);
			break;
		}
		case KeyEvent.VK_9: {
			this.worldPanel.setChoice(8);
			break;
		}
		default:
			break;
		}
	}
}