package org.serakos.ash.isleciv.draw;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.IntSummaryStatistics;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.Executors;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Timer;

import org.serakos.ash.isleciv.Civilization;
import org.serakos.ash.isleciv.Constants;
import org.serakos.ash.isleciv.World;
import org.serakos.ash.isleciv.WorldState;
import org.serakos.ash.isleciv.data.City;
import org.serakos.ash.isleciv.data.tile.TileData;

public class WorldPanel extends JPanel implements ActionListener
{
	private static final long serialVersionUID = 1421613291570269129L;

	protected BufferedImage io;
	protected World world;
	protected Random random;
	public Map<City, JFrame> frames;
	public int cold, hot, heightAverage, seasonAverage, windAverage, colorOrdinal;
	public boolean needsRedraw;
	public ColorChoices choice;

	public JScrollPane scrollRef;

	public WorldPanel(Random rand, World w)
	{
		this.random = rand;
		this.world = w;
		this.io = new BufferedImage(world.worldSize * 2, world.worldSize * 2, BufferedImage.TYPE_INT_RGB);
		this.frames = new HashMap<>();
		this.needsRedraw = true;
		this.choice = ColorChoices.STANDARD;
		this.colorOrdinal = choice.ordinal();
		setFocusable(true);
		addKeyListener(new KeyListener(this));
		addMouseListener(new MouseListener(this));
		setPreferredSize(new Dimension(io.getWidth(), io.getHeight()));
		Timer t = new Timer(10, this);
		t.start();
		Executors.newSingleThreadExecutor().execute(() -> {
			while (!world.generate())
				synchronized (WorldPanel.this) {
					needsRedraw = true;
					if (world.focusX != -1 && world.focusY != -1) {
						scrollRef.getHorizontalScrollBar().setValue(world.focusX * 2);
						scrollRef.getVerticalScrollBar().setValue(world.focusY * 2);
					}
					if (world.state == WorldState.TILES) {
						int layer = world.kingdomIndex.get();
						if (layer == 0)
							setChoice(ColorChoices.HEIGHT.ordinal());
						if (layer == 3)
							setChoice(ColorChoices.KINGDOM.ordinal());
						if (layer == 4)
							setChoice(ColorChoices.TEMPERATURE.ordinal());
						if (layer == 5)
							setChoice(ColorChoices.WIND.ordinal());
						if (layer == 6)
							setChoice(ColorChoices.SEASONS.ordinal());
					}
					if (world.state == WorldState.KINGDOMS)
						calcExtraData();
				}
			calcExtraData();
			needsRedraw = true;
			Civilization.setTitle("Done");
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (getGraphics() != null)
			paint(getGraphics());
	}

	@Override
	public void paint(Graphics g) {
		if (needsRedraw)
			redraw();
		g.drawImage(io, 0, 0, io.getWidth(), io.getHeight(), null);
	}

	public void redraw() {
		Graphics graph = io.getGraphics();
		graph.setColor(Color.black);
		graph.fillRect(0, 0, io.getWidth(), io.getHeight());
		if (world.state != WorldState.INIT
				&& (world.state != WorldState.TILES || world.kingdomIndex.get() > 0)) {
			for (int x = 0; x < world.worldSize; x++)
				for (int y = 0; y < world.worldSize; y++) {
					Color color = Optional.ofNullable(getSeaColor(x, y))
							.orElse(choice.func().getColor(this, world, x, y));
					graph.setColor(color);
					graph.fillRect(x * 2, y * 2, 2, 2);
				}
		}
		needsRedraw = false;

	}

	public Color getSeaColor(int x, int y) {
		if (choice == ColorChoices.KINGDOM || world.state == WorldState.INIT
				|| world.state == WorldState.TILES || choice == ColorChoices.HEIGHT)
			return null;

		int h = world.worldMap[x][y].get(TileData.HEIGHT);
		if (h < Constants.SEA_HEIGHT) {
			// if (world.worldMap[x][y].get(TileData.STRUCTURE) == Structure.ROAD.ordinal())
			// return Color.white;
			int n = (int) Math.min(Math.pow(h / 5 - h % 5, 2), 255);
			if (h > Constants.BEACH_HEIGHT)
				return new Color(n * 2, n * 2, n);
			if (world.worldMap[x][y].get(TileData.TEMPERATURE) < cold)
				return new Color(n, n, n * 2);
			return new Color(0, 0, n);
		}
		return null;
	}

	public void calcExtraData() {
		IntSummaryStatistics t = world.data.get(TileData.TEMPERATURE);
		int tdiff = t.getMax() - t.getMin();
		cold = (int) (tdiff / 3. + t.getMin());
		hot = (int) (t.getMax() - tdiff / 4.);
		heightAverage = (int) world.data.get(TileData.HEIGHT).getAverage();
		seasonAverage = (int) world.data.get(TileData.SEASONAL_CHANGE).getAverage();
		windAverage = (int) world.data.get(TileData.OCEAN_DOWNWIND).getAverage();

		choice = ColorChoices.STANDARD;
		colorOrdinal = choice.ordinal();
	}

	public void setChoice(int n) {
		n = Math.max(0, Math.min(ColorChoices.values().length - 1, n));
		this.colorOrdinal = n;
		this.choice = ColorChoices.values()[this.colorOrdinal];
		this.needsRedraw = true;
		// System.out.println("ColorChoice: " + this.worldPanel.choice);
	}
}
