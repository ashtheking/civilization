package org.serakos.ash.isleciv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import org.serakos.ash.isleciv.data.Kingdom;
import org.serakos.ash.isleciv.data.tile.GenLayer;
import org.serakos.ash.isleciv.data.tile.Tile;
import org.serakos.ash.isleciv.data.tile.TileData;
import org.serakos.ash.isleciv.util.PathBuilder;
import org.serakos.ash.isleciv.util.Utils;
import org.serakos.perlin.ImprovedNoise;

public class World
{
	public WorldState state;

	public Random random;
	public List<Kingdom> kingdoms;
	public Tile[][] worldMap;
	public int worldSize;
	public double worldZ;
	public Map<TileData, IntSummaryStatistics> data;
	public ThreadPoolExecutor threadPool;

	private ImprovedNoise noise;
	private List<GenLayer> layers;
	private CountDownLatch latch;
	private Utils.Time time;
	private Utils.Time kingdomTime;
	public AtomicInteger kingdomIndex;

	public int focusX, focusY;

	public World(long seed, int size)
	{
		this(new Random(seed), size);
	}

	public World(Random rand, int size)
	{
		this.random = rand;
		this.threadPool = new ThreadPoolExecutor(0, Runtime.getRuntime().availableProcessors(), 60L,
				TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
		this.state = WorldState.INIT;
		this.worldSize = size;
		this.time = new Utils.Time();
		this.kingdomTime = new Utils.Time();
		this.focusX = -1;
		this.focusY = -1;
	}

	public boolean generate() {
		if (state == WorldState.DONE)
			return true;
		switch (state) {
		case INIT: {
			time.startTime(0, state.output);
			noise = new ImprovedNoise(random.nextLong());
			worldMap = new Tile[worldSize][worldSize];
			layers = new ArrayList<>();
			kingdoms = new ArrayList<>();
			data = new TreeMap<>();
			latch = new CountDownLatch(worldSize * worldSize);
			kingdomIndex = new AtomicInteger(0);
			focusX = -1;
			focusY = -1;

			Stream.generate(() -> new Kingdom(this, (short) kingdoms.size())).limit(Constants.NUM_KINGDOMS)
					.forEach(kingdoms::add);
			time.endTime();
			state = WorldState.TILES;
			time.startTime(0, state.output);
			kingdomIndex.set(-1);
			break;
		}
		case TILES: {
			if (kingdomIndex.get() == -1) {
				kingdomTime.startTime(1, "Building layers.");
				genLayer(TileData.HEIGHT,
						(t, x, y) -> (noise.octave(x / 128.0, y / 128.0, worldZ, 8, 0.5) * 128) + 64);
				genLayer(TileData.WEIGHT, (t, x, y) -> t.belowSeaLevel(10 - t.get(TileData.HEIGHT) / 10,
						t.get(TileData.HEIGHT) / 5));
				genLayer(TileData.KINGDOM, (t, x, y) -> getVoronoi(x, y));
				genLayer(TileData.POPULATION_CHANCE,
						(t, x, y) -> t.belowSeaLevel(0, 250 * Math.pow(heightExp(t), -1)));
				genLayer(TileData.TEMPERATURE,
						(t, x, y) -> 60 * latitude(worldSize, y) - 6.5 * t.get(TileData.HEIGHT) / 25.0 + 20);
				genLayer(TileData.OCEAN_DOWNWIND,
						(t, x, y) -> 20 * Math.abs(Math.cos(3.0 * Math.PI * y / worldSize))
								+ t.belowSeaLevel(0.0, heightExp(t) - 1));
				genLayer(TileData.SEASONAL_CHANGE,
						(t, x, y) -> 25 * latitude(worldSize, 2 * y - worldSize / 2)
								+ t.get(TileData.OCEAN_DOWNWIND) / 4);
				kingdomIndex.incrementAndGet();
				kingdomTime.endTime();
				break;
			}
			int i = kingdomIndex.get();
			kingdomTime.startTime(1, "Generating layer " + layers.get(i).getData());
			for (int x = 0; x < worldSize; x++)
				for (int y = 0; y < worldSize; y++)
					threadPool.execute(gen(i, x, y));
			try {
				latch.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			kingdomIndex.incrementAndGet();
			kingdomTime.endTime();
			if (kingdomIndex.get() >= layers.size()) {
				buildStats();
				time.endTime();
				state = WorldState.KINGDOMS;
				time.startTime(0, state.output);
				kingdomIndex.set(0);
				focusX = -1;
				focusY = -1;
			}
			break;
		}
		case KINGDOMS: {
			Kingdom k = kingdoms.get(kingdomIndex.getAndIncrement());
			k.generate();
			focusX = k.getCity(0).x;
			focusY = k.getCity(0).y;
			if (kingdomIndex.get() >= kingdoms.size()) {
				time.endTime();
				state = WorldState.KINGDOM_ROADS;
				time.startTime(0, state.output);
				kingdomIndex.set(0);
				focusX = -1;
				focusY = -1;
				kingdomTime.startTime(1, "Building Roads for Kingdom of " + kingdoms.get(0).name);
			}
			break;
		}
		case KINGDOM_ROADS: {
			Kingdom k = kingdoms.get(kingdomIndex.get());
			focusX = k.getCity(0).x;
			focusY = k.getCity(0).y;
			if (!k.genRoads() && k.roadsGenerated) {
				kingdomIndex.incrementAndGet();
				kingdomTime.endTime();
				if (kingdomIndex.get() < kingdoms.size())
					kingdomTime.startTime(1,
							"Building Roads for Kingdom of " + kingdoms.get(kingdomIndex.get()).name);
			}
			if (kingdomIndex.get() >= kingdoms.size()) {
				time.endTime();
				state = WorldState.CONNECT;
				time.startTime(0, state.output);
				kingdomIndex.set(0);
				focusX = -1;
				focusY = -1;
			}
			break;
		}
		case CONNECT: {
			PathBuilder path = new PathBuilder(this);
			Kingdom k = kingdoms.get(kingdomIndex.get());
			focusX = k.getCity(0).x;
			focusY = k.getCity(0).y;
			kingdomTime.startTime(1, "Connecting " + k.name + " to all kingdoms.");
			Utils.Time cTime = new Utils.Time();
			for (int n = kingdomIndex.get() + 1; n < kingdoms.size(); n++) { // Loop
				Kingdom i = kingdoms.get(n); // Over all kingdoms not already connected
				cTime.startTime(2, "Connecting Kingdoms " + k.name + " and " + i.name); // Start Time
				path.buildPath(k.makeTilePair(i)); // Connect Them
				cTime.endTime(); // End Time
			}
			kingdomTime.endTime();
			if (kingdomIndex.incrementAndGet() >= kingdoms.size()) {
				time.endTime();
				state = WorldState.FINISHING;
				time.startTime(0, state.output);
				kingdomIndex.set(0);
				focusX = -1;
				focusY = -1;
			}
			break;
		}
		case FINISHING: {
			buildStats();
			data.forEach((d, s) -> System.out.println("" + d + ": " + s));
			time.endTime();
			state = WorldState.DONE;
			break;
		}
		default:
			throw new IllegalStateException("Unexpected state: " + state);
		}
		return false;
	}

	public void genLayer(TileData type, GenLayer.Supplier value) {
		layers.add(new GenLayer(type, value));
	}

	public static double heightExp(Tile t) {
		return Math.exp(t.get(TileData.HEIGHT) * 2.0 / Constants.SEA_HEIGHT);
	}

	public static double latitude(double size, int y) {
		return Math.sin(Math.PI * y / size);
	}

	private short getVoronoi(int x, int y) {
		short minId = -1;
		double minDist = Double.MAX_VALUE, d = -1;
		for (Kingdom k : kingdoms)
			if ((d = k.getDist(x, y)) < minDist) {
				minDist = d;
				minId = k.kingdomId;
			}
		return minId;
	}

	private Runnable gen(int l, int x, int y) {
		return () -> {
			Tile t = Optional.ofNullable(worldMap[x][y]).orElse(new Tile(x, y));
			worldMap[x][y] = t;
			layers.get(l).act(t, x, y);
			latch.countDown();
		};
	}

	private void buildStats() {
		Arrays.stream(TileData.values()).forEach(d -> data.put(d, Utils.streamWorld(this)
				.filter(t -> t.belowSeaLevel(false, true)).mapToInt(t -> t.get(d)).summaryStatistics()));
	}
}
