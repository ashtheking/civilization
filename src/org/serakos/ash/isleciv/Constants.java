package org.serakos.ash.isleciv;

public class Constants
{
	public static final int CITY_POP = 8000;

	public static final double CITY_SIZE = 6.0;
	public static final double TOWN_SIZE = 3.0;

	public static final int NUM_KINGDOMS = 8;

	public static final int SEA_HEIGHT = 56;
	public static final int BEACH_HEIGHT = SEA_HEIGHT - 2;

	public static final int WORLD_SIZE = 1024;

}
