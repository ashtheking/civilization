package org.serakos.ash.isleciv;

import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import org.serakos.ash.isleciv.draw.WorldPanel;

public class Civilization
{
	private static JFrame frame;

	public static void main(String[] args) {
		frame = new JFrame("Civilization");
		Random rand = new Random();
		WorldPanel isle = new WorldPanel(rand, new World(rand, Constants.WORLD_SIZE));
		JScrollPane pane = new JScrollPane(isle);
		isle.scrollRef = pane;
		frame.setContentPane(pane);
		frame.setSize(700, 800);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void setTitle(String s) {
		frame.setTitle("Civilization - " + s);
	}
}
